Il faut suivre ces étapes :

- Modifier dans le dossier etc les fichiers suivants :
  - debian_version
  - issue
  - issue.net
  - os-release

- Ensuite à la racine du dossier, exécutes la commande "sudo debuild"

Ca te compilera un fichier .deb installable avec "sudo dpkg -i" sur le
répertoire parent du dossier base-files.
